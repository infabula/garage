import sys
from vehicles.vehicle import Vehicle
#from spacetoys import Vehicle as SpaceVehicle


def main():
    print("Let's create a car")

    print(Vehicle.FUEL_TYPES[0])
    
    car = Vehicle(1500.0)
    mini = Vehicle(750.0)
    print("Mass:", car.mass)

    sys.exit(0)

if __name__ == "__main__":
    main()
