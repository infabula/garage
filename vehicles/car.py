from vehicle import Vehicle
from engine import Engine

class Car(Vehicle):
    
    def __init__(self, mass, brand, horse_power, engine):
        super().__init__(mass)
        self.brand = brand
        self.max_nb_passengers = 4
        self.nb_of_passengers = 0
        self.engine = engine


    def __str__(self):
        parent_rep = super().__str__()
        parent_rep += self.brand
        parent_rep += "number of passengers: " + str(self.nb_of_passengers)
        return parent_rep
        
        
def some_testing():
    car_engine = Engine(horse_power=120.0,
                    fuel_type=Vehicle.FUEL_TYPES[0])
    car = Car(mass=1200, brand="Volvo", horse_power=120, engine=car_engine)

    print(car)
        
if __name__ == "__main__":
    some_testing()
        
