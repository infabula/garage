from vehicle import Vehicle

class Engine:
    def __init__(self, horse_power, fuel_type):
        self.horse_power = horse_power
        self.fuel_type = fuel_type
        self.is_running = False

    def start(self):
        """Starts the engine if not running"""
        if not self.is_running:
            print("Starting the engine")
            self.is_running = True
        else:
            print("Engine is already running")



def some_testing():
    engine = Engine(horse_power=120.0,
                    fuel_type=Vehicle.FUEL_TYPES[0])
    engine.start()
                  
                  
if __name__ == "__main__":
    some_testing()

