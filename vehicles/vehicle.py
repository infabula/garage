class Vehicle:
    """Vehicle base class"""
    FUEL_TYPES = ("petrol", "LPG", "CNG", "Diesel")
    
    def __init__(self, mass):
        print("Initializing a vehicle")
        self.licenceplate = "-----"
        self.owner = None
        self.km = 0.0
        self.mass = mass  # KG
        self.fueltype = "petrol"
        self.nb_of_wheels = 4

    def __str__(self):
        return self.licenceplate
        
    @property 
    def mass(self):
        return self._mass

    @mass.setter
    def mass(self, mass):
        if mass > 0.0:
            self._mass = mass
        else:
            raise ValueError("Mass should be positive")
